package jdbcdemo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frame;
	private JTextField username;
	private JTextField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(65, 101, 112, 22);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(65, 151, 112, 22);
		frame.getContentPane().add(lblPassword);
		
		username = new JTextField();
		username.setBounds(187, 102, 154, 20);
		frame.getContentPane().add(username);
		username.setColumns(10);
		
		passwordField = new JTextField();
		passwordField.setBounds(187, 152, 154, 20);
		frame.getContentPane().add(passwordField);
		passwordField.setColumns(10);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String uname = username.getText();
				String pswd = passwordField.getText();
				
				if (uname.equals("name") && pswd.equals("password"))
				{
					JOptionPane.showMessageDialog(frame, "You have successfully logged in.");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "Invalid username or password.");
				}
			}
		});
		btnLogin.setBounds(168, 208, 104, 23);
		frame.getContentPane().add(btnLogin);
	}
}
