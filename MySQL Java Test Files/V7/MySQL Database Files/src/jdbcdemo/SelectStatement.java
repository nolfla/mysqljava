package jdbcdemo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class SelectStatement {

	private JFrame frame;
	private JTextField txtUserInput;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SelectStatement window = new SelectStatement();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SelectStatement() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextPane txtpnDatabaseChoice = new JTextPane();
		txtpnDatabaseChoice.setText("Type in your database choice and press enter");
		txtpnDatabaseChoice.setBounds(10, 64, 154, 40);
		frame.getContentPane().add(txtpnDatabaseChoice);
		
		JTextPane txtpnSelectDataFrom = new JTextPane();
		txtpnSelectDataFrom.setText("Select Data From MySQL Databases");
		txtpnSelectDataFrom.setBounds(97, 11, 197, 20);
		frame.getContentPane().add(txtpnSelectDataFrom);
		
		txtUserInput = new JTextField();
		txtUserInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String userInput = txtUserInput.getText();
				
				if (userInput.equals("categories"))
				{
					JOptionPane.showMessageDialog(frame, "Categories Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "Categories Incorrect");
				}
				
				
				if (userInput.equals("desktops"))
				{
					JOptionPane.showMessageDialog(frame, "Desktops Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "Desktops Incorrect");
				}
				
				if (userInput.equals("inventory"))
				{
					JOptionPane.showMessageDialog(frame, "inventory Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "inventory Incorrect");
				}
				
				if (userInput.equals("laptops"))
				{
					JOptionPane.showMessageDialog(frame, "laptops Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "laptops Incorrect");
				}
				
				if (userInput.equals("manufacturers"))
				{
					JOptionPane.showMessageDialog(frame, "manufacturers Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "manufacturers Incorrect");
				}
				
				if (userInput.equals("merchants"))
				{
					JOptionPane.showMessageDialog(frame, "merchants Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "merchants Incorrect");
				}
				
				if (userInput.equals("monitors"))
				{
					JOptionPane.showMessageDialog(frame, "monitors Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "monitors Incorrect");
				}
				
				if (userInput.equals("routers"))
				{
					JOptionPane.showMessageDialog(frame, "routers Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "routers Incorrect");
				}
				
				if (userInput.equals("servers"))
				{
					JOptionPane.showMessageDialog(frame, "servers Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "servers Incorrect");
				}
				
				if (userInput.equals("switches"))
				{
					JOptionPane.showMessageDialog(frame, "switches Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "switches Incorrect");
				}
				
				if (userInput.equals("tablets"))
				{
					JOptionPane.showMessageDialog(frame, "tablets Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "tablets Incorrect");
				}
				
				if (userInput.equals("thinclients"))
				{
					JOptionPane.showMessageDialog(frame, "thinclients Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "thinclients Incorrect");
				}
				
				if (userInput.equals("tvs"))
				{
					JOptionPane.showMessageDialog(frame, "tvs Test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "tvs Incorrect");
				}
			}
		});
		txtUserInput.setBounds(186, 64, 136, 20);
		frame.getContentPane().add(txtUserInput);
		txtUserInput.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(97, 132, 184, 20);
		frame.getContentPane().add(comboBox);
	}
}
