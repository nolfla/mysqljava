package jdbcdemo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SelectStatementV2 {

	private JFrame frame;
	private JTextField txtCategories;
	private JTextField txtInventory;
	private JTextField txtManufacturers;
	private JTextField txtMerchants;
	private JTextField txtRouters;
	private JTextField txtServers;
	private JTextField txtSwitches;
	private JTextField txtTablets;
	private JTextField txtThinclients;
	private JTextField txtTvs;
	private JTextField txtLaptops;
	private JTextField txtMonitors;
	private JTextField txtDesktops;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SelectStatementV2 window = new SelectStatementV2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SelectStatementV2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSelectStatements = new JLabel("Select Statements");
		lblSelectStatements.setBounds(127, 11, 166, 35);
		frame.getContentPane().add(lblSelectStatements);
		
		txtCategories = new JTextField();
		txtCategories.addActionListener(new ActionListener() {
			
			
			
			public void actionPerformed(ActionEvent arg0) {
				
				String categoriesinput = txtCategories.getText();
				
				if (categoriesinput.equals("categories"))
				{
					JOptionPane.showMessageDialog(frame, "categories test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			}
		});
		txtCategories.setText("categories");
		txtCategories.setBounds(65, 57, 86, 20);
		frame.getContentPane().add(txtCategories);
		txtCategories.setColumns(10);
		
		txtInventory = new JTextField();
		txtInventory.addActionListener(new ActionListener() {
			
			
			
			public void actionPerformed(ActionEvent e) {
				
				String inventoryinput = txtInventory.getText();
				
				if (inventoryinput.equals("inventory"))
				{
					JOptionPane.showMessageDialog(frame, "inventory test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
				
			}
		});
		txtInventory.setText("inventory");
		txtInventory.setBounds(281, 57, 86, 20);
		frame.getContentPane().add(txtInventory);
		txtInventory.setColumns(10);
		
		txtManufacturers = new JTextField();
		txtManufacturers.addActionListener(new ActionListener() {
			
			
			
			public void actionPerformed(ActionEvent e) {
				
				String manufacturersinput = txtManufacturers.getText();
				
				if (manufacturersinput.equals("manufacturers"))
				{
					JOptionPane.showMessageDialog(frame, "manufacturers test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
				
			}
		});
		txtManufacturers.setText("manufacturers");
		txtManufacturers.setBounds(168, 107, 86, 20);
		frame.getContentPane().add(txtManufacturers);
		txtManufacturers.setColumns(10);
		
		txtMerchants = new JTextField();
		txtMerchants.addActionListener(new ActionListener() {
			
			
			
			public void actionPerformed(ActionEvent e) {
				
				String merchantsinput = txtMerchants.getText();
				
				if (merchantsinput.equals("merchants"))
				{
					JOptionPane.showMessageDialog(frame, "merchants test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
				
			}
		});
		txtMerchants.setText("merchants");
		txtMerchants.setBounds(281, 107, 86, 20);
		frame.getContentPane().add(txtMerchants);
		txtMerchants.setColumns(10);
		
		txtRouters = new JTextField();
		txtRouters.addActionListener(new ActionListener() {
					
			
			public void actionPerformed(ActionEvent e) {
				
				String routersinput = txtRouters.getText();
				
				if (routersinput.equals("routers"))
				{
					JOptionPane.showMessageDialog(frame, "routers test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			}
		});
		txtRouters.setText("routers");
		txtRouters.setBounds(168, 151, 86, 20);
		frame.getContentPane().add(txtRouters);
		txtRouters.setColumns(10);
		
		txtServers = new JTextField();
		txtServers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String serversinput = txtServers.getText();
				
				if (serversinput.equals("servers"))
				{
					JOptionPane.showMessageDialog(frame, "servers");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			}
		});
		txtServers.setText("servers");
		txtServers.setBounds(281, 151, 86, 20);
		frame.getContentPane().add(txtServers);
		txtServers.setColumns(10);
		
		txtSwitches = new JTextField();
		txtSwitches.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String switchesinput = txtSwitches.getText();
				
				if (switchesinput.equals("switches"))
				{
					JOptionPane.showMessageDialog(frame, "switches test");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			}
		});
		txtSwitches.setText("switches");
		txtSwitches.setBounds(65, 195, 86, 20);
		frame.getContentPane().add(txtSwitches);
		txtSwitches.setColumns(10);
		
		txtTablets = new JTextField();
		txtTablets.addActionListener(new ActionListener() {
			
			
			
			public void actionPerformed(ActionEvent e) {
				
				String tabletsinput = txtTablets.getText();
				
				
				
				if (tabletsinput.equals("tablets"))
				{
					JOptionPane.showMessageDialog(frame, "tablets");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			}
		});
		txtTablets.setText("tablets");
		txtTablets.setBounds(168, 195, 86, 20);
		frame.getContentPane().add(txtTablets);
		txtTablets.setColumns(10);
		
		txtThinclients = new JTextField();
		txtThinclients.addActionListener(new ActionListener() {
			
			
			
			public void actionPerformed(ActionEvent e) {
				
				String thinclientsinput = txtThinclients.getText();
				
				
				if (thinclientsinput.equals("thinclients"))
				{
					JOptionPane.showMessageDialog(frame, "thinclients");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			}
		});
		txtThinclients.setText("thinclients");
		txtThinclients.setBounds(281, 195, 86, 20);
		frame.getContentPane().add(txtThinclients);
		txtThinclients.setColumns(10);
		
		txtTvs = new JTextField();
		txtTvs.addActionListener(new ActionListener() {
			
			
			
			public void actionPerformed(ActionEvent e) {
				
				
				String tvsinput = txtTvs.getText();
				
				if (tvsinput.equals("tvs"))
				{
					JOptionPane.showMessageDialog(frame, "tvs");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			}
		});
		txtTvs.setText("tvs");
		txtTvs.setBounds(65, 226, 86, 20);
		frame.getContentPane().add(txtTvs);
		txtTvs.setColumns(10);
		
		txtLaptops = new JTextField();
		txtLaptops.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String laptopsinput = txtLaptops.getText();
				
				if (laptopsinput.equals("laptops"))
				{
					JOptionPane.showMessageDialog(frame, "laptops");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			
			}
		});
		txtLaptops.setText("laptops");
		txtLaptops.setBounds(65, 107, 86, 20);
		frame.getContentPane().add(txtLaptops);
		txtLaptops.setColumns(10);
		
		txtMonitors = new JTextField();
		txtMonitors.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String monitorsinput = txtMonitors.getText();
				
				if (monitorsinput.equals("monitors"))
				{
					JOptionPane.showMessageDialog(frame, "monitors");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			
			}
		});
		txtMonitors.setText("monitors");
		txtMonitors.setBounds(65, 151, 86, 20);
		frame.getContentPane().add(txtMonitors);
		txtMonitors.setColumns(10);
		
		txtDesktops = new JTextField();
		txtDesktops.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String desktopsinput = txtDesktops.getText();
				
				if (desktopsinput.equals("desktops"))
				{
					JOptionPane.showMessageDialog(frame, "desktops");
				}
				
				else
				{
					JOptionPane.showMessageDialog(frame, "incorrect table name");
				}
				
			
			}
		});
		txtDesktops.setText("desktops");
		txtDesktops.setBounds(168, 57, 86, 20);
		frame.getContentPane().add(txtDesktops);
		txtDesktops.setColumns(10);
	}
}
